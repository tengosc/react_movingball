import React, { useCallback, useEffect } from "react";

const CallbackResult = ({
  elPos,
  reverse,
  color,
  ballObj,
  setColorToggle,
  setColor,
  colorObj,
}) => {
  const testFunction = useCallback(() => {
    setColor(colorObj.color);
    setColorToggle(false);
  }, [reverse]);

  useEffect(() => {
    testFunction();
    console.log("test function changed");
  }, [reverse]);

  const changeColor = () => {
    ballObj.color = color;
    setColorToggle(true);
    console.log("color", ballObj.color);
  };

  return (
    <div>
      <p>top: {elPos.y} px</p>
      <p>left: {elPos.x} px</p>
      <button onClick={() => changeColor()}>set color</button>
    </div>
  );
};

export default CallbackResult;
