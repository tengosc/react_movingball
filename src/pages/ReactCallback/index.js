import React, { useEffect, useRef, useState } from "react";
import CallbackResult from "./CallbackResult";

const ballObj = {
  x: 20,
  y: 20,
  dx: 5,
  dy: 5,
  rad: 15,
  color: "blue",
};

const colorObj = {
  color: "red",
};

class Ball {
  constructor(x, y, rad, color) {
    this.x = x;
    this.y = y;
    this.rad = rad;
    this.color = color;
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.rad, 0, 2 * Math.PI);
    ctx.strokeStyle = "black";
    ctx.strokeWidth = 4;
    ctx.fill();
    ctx.stroke();
  }
}

function ReactCallback() {
  const [elPos, setElPos] = useState({ x: ballObj.x, y: ballObj.y });
  const [reverse, setReverse] = useState(false);
  const [color, setColor] = useState(ballObj.color);
  const [colorToggle, setColorToggle] = useState(false);
  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    let data = new Ball(ballObj.x, ballObj.y, ballObj.rad, ballObj.color);
    data.draw(ctx);
  }, []);

  useEffect(() => {
    if (!colorToggle) {
      ballObj.color = colorObj.color;
    } else {
      ballObj.color = color;
    }
  }, [colorObj.color]);

  const render = () => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    let data = new Ball(ballObj.x, ballObj.y, ballObj.rad, ballObj.color);
    data.draw(ctx);

    ballObj.x += ballObj.dx;
    ballObj.y += ballObj.dy;

    colorObj.color = `hsl(${ballObj.y},100%, 50%)`;

    setElPos({ x: ballObj.x, y: ballObj.y });

    if (
      ballObj.y - ballObj.rad <= 0 ||
      ballObj.y + ballObj.rad >= canvas.height
    ) {
      ballObj.dy *= -1;
    }

    if (
      ballObj.x + ballObj.rad >= canvas.width ||
      ballObj.x - ballObj.rad <= 0
    ) {
      ballObj.dx *= -1;
    }

    if (ballObj.x + ballObj.rad >= canvas.width) {
      setReverse(true);
    } else if (ballObj.x - ballObj.rad <= 0) {
      setReverse(false);
    }

    requestAnimationFrame(render);
  };

  return (
    <div>
      <canvas height="255" width="600" ref={canvasRef} />
      <div>
        <button type="button" onClick={render}>
          Start
        </button>
        <CallbackResult
          {...{
            elPos,
            reverse,
            color,
            ballObj,
            setColorToggle,
            setColor,
            colorObj,
          }}
        />
      </div>
    </div>
  );
}

export default ReactCallback;
