import React, { useEffect, useState } from "react";

const RectangleResult = ({ myRef }) => {
  const dimensions = myRef?.current?.attributes;
  const [area, setArea] = useState(0);

  useEffect(() => {
    setArea(dimensions?.width?.value * dimensions?.height?.value);
  }, [dimensions?.width?.value, dimensions?.height?.value]);

  return <div className="rectangle--text">Pole wynosi {area} px</div>;
};

export default RectangleResult;
