// import logo from "./logo.svg";
import "./App.css";
import React from "react";
// import Rectangle from "./pages/rectangle";
import ReactCallback from "./pages/ReactCallback";

function App() {
  return (
    <div className="App">
      <ReactCallback />
    </div>
  );
}

export default App;
